module.exports = function(url, options){
  if (options.method) {
    var method = options.method.toUpperCase();
    if (method !== 'GET' || method !== 'POST'){
      var delimiter = url.indexOf("?") >= 0 ? '&' : '?';
      options.method = 'POST';
      return fetch(url + delimiter + '_method=' + method, options);
    } else {
      return fetch(url, options);
    }
  } else {
    return fetch(url, options);
  }
};
