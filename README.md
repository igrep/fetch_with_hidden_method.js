# `fetch_with_hidden_method.js`

## SYNOPSIS

Wrap `fetch` method to set `_method` parameter automatically.

```javascript
import fetch from "fetch_with_hidden_method";

// Use as ordinary fetch...
fetch('/foo/bar', { method: 'put' })
    .then(() => {
        console.log("The server would receive a request to '/foo/bar?_method=PUT' as POST.";
    }));
```
